<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

class BaseTransformer extends TransformerAbstract
{
    /**
     * Transform an object into a jsonable array
     *
     * @param mixed $model
     * @return array
     * @throws \Exception
     */
    public function transform($object)
    {
        if (is_object($object) && $object instanceof Model) {
            $transformed = $object->toArray();
        } elseif (is_object($object) && $object instanceof Collection) {
            $transformed = $object->items;
        } else {
            throw new \Exception('Unexpected object type encountered in transformer');
        }
        return $transformed;
    }
}
