<?php

namespace App\Console\Base;

use App\Helpers\General\BitbucketHelper;
use Illuminate\Console\Command;
use PhpZip\ZipFile;
use Symfony\Component\Filesystem\Filesystem;

class BaseCommand extends Command {
    protected $filesystem;
    protected $zip;
    protected $bitbucket;

    public function __construct() {
        parent::__construct();
        $this->filesystem = new Filesystem();
        $this->zip = new ZipFile();
        $this->bitbucket = new BitbucketHelper();
    }
}
