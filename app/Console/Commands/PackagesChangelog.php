<?php

namespace App\Console\Commands;

use App\Handlers\TelegramHandler;
use App\Models\Plugin;
use Illuminate\Console\Command;
use DiDom\Document;
use Illuminate\Support\Facades\Log;
use League\HTMLToMarkdown\HtmlConverter;
use Monolog\Logger;

class PackagesChangelog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'packages:changelog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate changelog for packages';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $package = new Plugin();

        $markdown_converter = new HtmlConverter();

        $markdown_converter->getConfig()->setOption('bold_style', '**');

        $package_current = $package->where('slug', 'elementor-pro')->first();

        $document = new Document($package_current->changelog_page, true);

        $changelog_page_text = $document->text();
        $changelog_page_hash_current = md5($changelog_page_text);

        $changelog_page_hash = $package_current->changelog_page_hash;
        $changelog_page_last_version = $package_current->changelog_page_last_version;

        Log::channel('commands')->info('Task -> Changelog');

        Log::channel('commands')->info('Old hash: ' . $changelog_page_hash . ' New Hash:' . $changelog_page_hash_current);

        if ($changelog_page_hash_current === $changelog_page_hash) {
            $this->error('No changes(hash)');
            return false;
        }

        $changelog_page_item_title = $document->first('.elementor-widget-theme-post-content .elementor-widget-container h4')->innerHtml();

        $changelog_page_item_text = $document->first('.elementor-widget-theme-post-content .elementor-widget-container ul')->html();

        $changelog['date'] = $this->elementor_pro_processor($changelog_page_item_title, $changelog_page_item_text)['date'];
        $changelog['version'] = $this->elementor_pro_processor($changelog_page_item_title, $changelog_page_item_text)['version'];

        if ($changelog['version'] === $changelog_page_last_version) {
            $this->error('No changes(version)');
            return false;
        }

        $package_current->changelog_page_hash = $changelog_page_hash_current;
        $package_current->changelog_page_last_version = $changelog['version'];

        $package_current->save();

        $log = new Logger('TelegramHandler');
        $log->pushHandler(new TelegramHandler(config('tgbot.tgbot_token'), config('tgbot.tgbot_channel'),'UTC','F j, Y, g:i a',60));
        $log->info("*" . $this->elementor_pro_processor($changelog_page_item_title, $changelog_page_item_text)['title'] . "*\n" . $changelog['date'] . "\n*" . $changelog['version'] . "*\n" . $markdown_converter->convert($changelog_page_item_text));

    }

    public function elementor_pro_processor($title, $text) {
        $date = substr($title, -10, 10);
        $version = str_replace([$date, ' ', '-'], '', $title);
        return [
            'title' => 'Elementor Pro',
            'date' => $date,
            'version' => $version
        ];
    }
}
