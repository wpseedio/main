<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PackagesUpload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'packages:upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload packages from sftp to main folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $source = Storage::disk('sftp');
        $source_files = $source->allFiles();

        foreach ($source_files as $source_files_item) {
            $source_current_mimetype = Storage::disk('sftp')->getMimetype($source_files_item);
            if ('application/zip' !== $source_current_mimetype) {
                continue;
            }
            $this->info('Upload file: ' . $source_files_item);
            Storage::disk('packages')->put($source_files_item, Storage::disk('sftp')->get($source_files_item));
            $this->info('Delete file: ' . $source_files_item);
            Storage::disk('sftp')->delete($source_files_item);
        }
    }
}
