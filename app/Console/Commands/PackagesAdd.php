<?php

namespace App\Console\Commands;

use App\Console\Base\BaseCommand;
use App\Models\Package;
use App\Models\PackageVersion;
use App\Models\Plugin;
use App\Models\PluginVersion;
use App\Models\Theme;
use App\Models\ThemeVersion;
use Cz\Git\GitRepository;
use Illuminate\Support\Facades\Log;

class PackagesAdd extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'packages:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add packages(plugins and themes) to core';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Task -> Add packages(plugins and themes) to core started...');

        Log::channel('commands')->info('Task -> Add packages(plugins and themes) to core');

        $folder_upload = base_path(config('packages.upload'));
        if (is_link($folder_upload)) {
            $folder_upload = readlink($folder_upload);
        }
        $file_list = scandir($folder_upload);

        $bitbucket_account_plugins = config('bitbucket.accounts.plugins');
        $bitbucket_account_themes = config('bitbucket.accounts.themes');

        $this->filesystem->remove(base_path(config('packages.extract')));

        foreach ($file_list as $file_item) {
            $file_path = $folder_upload . '/' . $file_item;
            $file_ext = pathinfo($file_path, PATHINFO_EXTENSION);
            if (! is_file($file_path)) {
                continue;
            }
            $this->info('> Try add package with name: ' . $file_item);
            if ('zip' !== $file_ext) {
                $this->error('File Error: File extension is not a zip');
                $this->filesystem->remove($file_path);
                continue;
            }
            $package = new \Max_WP_Package($file_path);
            $package_type = $package->get_type();
            /** @todo Check $package_type */
            $bitbucket_account = $bitbucket_account_plugins;
            if ('theme' === $package_type) {
                $bitbucket_account = $bitbucket_account_themes;
            }
            $package_metadata = $package->get_metadata();
            $slug = preg_replace('/\s/', '', $package_metadata['slug']);

            $this->info('Package: ' . $slug . '(version ' . $package_metadata['version'] . '), type: ' . $package_type);
            $file_hash_sha256 = hash_file('sha256', $file_path);
            $file_hash_sha1 = hash_file('sha1', $file_path);
            $file_hash_md5 = hash_file('md5', $file_path);
            $extract_path = base_path(config('packages.extract') . '/' . $file_hash_sha256);
            $this->filesystem->mkdir($extract_path, 0755);

            $this->bitbucket->create($bitbucket_account, $slug, substr($package_metadata['description'], 0, 755));

            try {
                $git_repo = GitRepository::init($extract_path . '/' . $slug);
                $git_repo->addRemote(
                    'origin',
                    'git@bitbucket.org:' . $bitbucket_account . '/' . $slug . '.git'
                );
                $git_repo->fetch(null, ['--all']);
                $git_repo_tags = $git_repo->getTags() ? $git_repo->getTags() : [];
            } catch (\Throwable $t) {
                $this->error('Repository Fatal Error: ' . $t->getMessage());
            }

            if (in_array('v' . $package_metadata['version'], $git_repo_tags)) {
                $this->error('Repository Error: Package version exists');
                $this->filesystem->remove($file_path);
                continue;
            }

            try {
                if (! empty($git_repo_tags)) {
                    $git_repo->checkout('master');
                    $git_repo->pull();
                }
                $zipFile = $this->zip->openFile($file_path);
                $zipFile->extractTo($extract_path);
                $git_repo->addAllChanges();
                $git_repo->commit('v' . $package_metadata['version']);
                $git_repo->createTag(
                    'v' . $package_metadata['version'],
                    ['-m' => 'v' . $package_metadata['version']]
                );
                $git_repo->push('origin master');
                $git_repo->push('origin master --tags');
                $this->info('Package version added to repository');
            } catch (\Throwable $t) {
                $this->error('Repository Fatal Error:' . $t->getMessage());
            } finally {
                $zipFile->close();
            }

            $this->db_package_insert($package_metadata['name'], $slug, $package_type, $package_metadata['description']);
            $this->db_package_version_insert($slug, $package_type, $package_metadata['version'], $file_hash_sha256, $file_hash_sha1, $file_hash_md5);
            $this->info('Package version added to database');

            Log::channel('commands')->info('Package ' . $file_item . ' added successfully');

            $this->filesystem->remove($file_path);
        }
    }

    public function db_package_insert($name, $slug, $type, $description)
    {
        $package = new Plugin();
        if ('theme' === $type) {
            $package = new Theme();
        }

        $package_exists = $package->where('slug', $slug)->first();

        if ($package_exists) {
            $this->error('Database Error: Package already exists');
            return false;
        }

        $package->name = $name;
        $package->slug = $slug;
        $package->description = $description;

        $package->save();
        $this->info('Database: Package successfully added to database');
        return true;
    }

    public function db_package_version_insert($slug, $type, $version, $hash_sha256, $hash_sha1, $hash_md5)
    {
        $package = new Plugin();
        $package_version = new PluginVersion();
        if ('theme' === $type) {
            $package = new Theme();
            $package_version = new ThemeVersion();
        }

        $package_current = $package->where('slug', $slug)->first();
        $package_current_id = $package_current->id;
        if ('plugin' === $type) {
            $package_version->plugin_id = $package_current_id;
        } else {
            $package_version->theme_id = $package_current_id;
        }
        $package_version->version = $version;
        $package_version->hash_sha256 = $hash_sha256;
        $package_version->hash_sha1 = $hash_sha1;
        $package_version->hash_md5 = $hash_md5;

        $package_version->save();
    }
}
