<?php

namespace App\Events\Backend\Theme;

use Illuminate\Queue\SerializesModels;

/**
 * Class ThemeCreated.
 */
class ThemeCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $themes;

    /**
     * @param $themes
     */
    public function __construct($themes)
    {
        $this->themes = $themes;
    }
}
