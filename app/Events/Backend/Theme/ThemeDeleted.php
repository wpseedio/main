<?php

namespace App\Events\Backend\Theme;

use Illuminate\Queue\SerializesModels;

/**
 * Class ThemeDeleted.
 */
class ThemeDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $themes;

    /**
     * @param $themes
     */
    public function __construct($themes)
    {
        $this->themes = $themes;
    }
}
