<?php

namespace App\Events\Backend\Theme;

use Illuminate\Queue\SerializesModels;

/**
 * Class ThemeUpdated.
 */
class ThemeUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $themes;

    /**
     * @param $themes
     */
    public function __construct($themes)
    {
        $this->themes = $themes;
    }
}
