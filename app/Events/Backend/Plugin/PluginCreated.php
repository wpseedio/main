<?php

namespace App\Events\Backend\Plugin;

use Illuminate\Queue\SerializesModels;

/**
 * Class PluginCreated.
 */
class PluginCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $plugins;

    /**
     * @param $plugins
     */
    public function __construct($plugins)
    {
        $this->plugins = $plugins;
    }
}
