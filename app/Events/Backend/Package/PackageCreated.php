<?php

namespace App\Events\Backend\Package;

use Illuminate\Queue\SerializesModels;

/**
 * Class PackageCreated.
 */
class PackageCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $packages;

    /**
     * @param $packages
     */
    public function __construct($packages)
    {
        $this->packages = $packages;
    }
}
