<?php

namespace App\Events\Backend\Package;

use Illuminate\Queue\SerializesModels;

/**
 * Class PackageDeleted.
 */
class PackageDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $packages;

    /**
     * @param $packages
     */
    public function __construct($packages)
    {
        $this->packages = $packages;
    }
}
