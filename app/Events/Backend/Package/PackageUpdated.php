<?php

namespace App\Events\Backend\Package;

use Illuminate\Queue\SerializesModels;

/**
 * Class PackageUpdated.
 */
class PackageUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $packages;

    /**
     * @param $packages
     */
    public function __construct($packages)
    {
        $this->packages = $packages;
    }
}
