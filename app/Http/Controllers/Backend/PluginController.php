<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use App\Models\Plugin;
use App\Repositories\Backend\PluginRepository;
use App\Http\Requests\Backend\Plugin\ManagePluginRequest;
use App\Http\Requests\Backend\Plugin\StorePluginRequest;
use App\Http\Requests\Backend\Plugin\UpdatePluginRequest;

use App\Events\Backend\Plugin\PluginCreated;
use App\Events\Backend\Plugin\PluginUpdated;
use App\Events\Backend\Plugin\PluginDeleted;

class PluginController extends Controller
{
    /**
     * @var PluginRepository
     */
    protected $pluginRepository;

    /**
     * PluginController constructor.
     *
     * @param PluginRepository $pluginRepository
     */
    public function __construct(PluginRepository $pluginRepository)
    {
        $this->pluginRepository = $pluginRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManagePluginRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManagePluginRequest $request)
    {
        return view('backend.plugin.index')
            ->withplugins($this->pluginRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManagePluginRequest    $request
     *
     * @return mixed
     */
    public function create(ManagePluginRequest $request)
    {
        return view('backend.plugin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePluginRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StorePluginRequest $request)
    {
        $this->pluginRepository->create($request->only(
            'title'
        ));

        // Fire create event (PluginCreated)
        event(new PluginCreated($request));

        return redirect()->route('admin.plugins.index')
            ->withFlashSuccess(__('backend_plugins.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManagePluginRequest  $request
     * @param Plugin               $plugin
     *
     * @return mixed
     */
    public function show(ManagePluginRequest $request, Plugin $plugin)
    {
        return view('backend.plugin.show')->withPlugin($plugin);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManagePluginRequest $request
     * @param Plugin              $plugin
     *
     * @return mixed
     */
    public function edit(ManagePluginRequest $request, Plugin $plugin)
    {
        return view('backend.plugin.edit')->withPlugin($plugin);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePluginRequest  $request
     * @param Plugin               $plugin
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdatePluginRequest $request, Plugin $plugin)
    {
        $this->pluginRepository->update($plugin, $request->only(
            'title'
        ));

        // Fire update event (PluginUpdated)
        event(new PluginUpdated($request));

        return redirect()->route('admin.plugins.index')
            ->withFlashSuccess(__('backend_plugins.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManagePluginRequest $request
     * @param Plugin              $plugin
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManagePluginRequest $request, Plugin $plugin)
    {
        $this->pluginRepository->deleteById($plugin->id);

        // Fire delete event (PluginDeleted)
        event(new PluginDeleted($request));

        return redirect()->route('admin.plugins.deleted')
            ->withFlashSuccess(__('backend_plugins.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManagePluginRequest $request
     * @param Plugin              $deletedPlugin
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManagePluginRequest $request, Plugin $deletedPlugin)
    {
        $this->pluginRepository->forceDelete($deletedPlugin);

        return redirect()->route('admin.plugins.deleted')
            ->withFlashSuccess(__('backend_plugins.alerts.deleted_permanently'));
    }

    /**
     * @param ManagePluginRequest $request
     * @param Plugin              $deletedPlugin
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManagePluginRequest $request, Plugin $deletedPlugin)
    {
        $this->pluginRepository->restore($deletedPlugin);

        return redirect()->route('admin.plugins.index')
            ->withFlashSuccess(__('backend_plugins.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManagePluginRequest $request
     *
     * @return mixed
     */
    public function deleted(ManagePluginRequest $request)
    {
        return view('backend.plugin.deleted')
            ->withplugins($this->pluginRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
