<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use App\Models\Theme;
use App\Repositories\Backend\ThemeRepository;
use App\Http\Requests\Backend\Theme\ManageThemeRequest;
use App\Http\Requests\Backend\Theme\StoreThemeRequest;
use App\Http\Requests\Backend\Theme\UpdateThemeRequest;

use App\Events\Backend\Theme\ThemeCreated;
use App\Events\Backend\Theme\ThemeUpdated;
use App\Events\Backend\Theme\ThemeDeleted;

class ThemeController extends Controller
{
    /**
     * @var ThemeRepository
     */
    protected $themeRepository;

    /**
     * ThemeController constructor.
     *
     * @param ThemeRepository $themeRepository
     */
    public function __construct(ThemeRepository $themeRepository)
    {
        $this->themeRepository = $themeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManageThemeRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageThemeRequest $request)
    {
        return view('backend.theme.index')
            ->withthemes($this->themeRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManageThemeRequest    $request
     *
     * @return mixed
     */
    public function create(ManageThemeRequest $request)
    {
        return view('backend.theme.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreThemeRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreThemeRequest $request)
    {
        $this->themeRepository->create($request->only(
            'title'
        ));

        // Fire create event (ThemeCreated)
        event(new ThemeCreated($request));

        return redirect()->route('admin.themes.index')
            ->withFlashSuccess(__('backend_themes.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManageThemeRequest  $request
     * @param Theme               $theme
     *
     * @return mixed
     */
    public function show(ManageThemeRequest $request, Theme $theme)
    {
        return view('backend.theme.show')->withTheme($theme);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManageThemeRequest $request
     * @param Theme              $theme
     *
     * @return mixed
     */
    public function edit(ManageThemeRequest $request, Theme $theme)
    {
        return view('backend.theme.edit')->withTheme($theme);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateThemeRequest  $request
     * @param Theme               $theme
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateThemeRequest $request, Theme $theme)
    {
        $this->themeRepository->update($theme, $request->only(
            'title'
        ));

        // Fire update event (ThemeUpdated)
        event(new ThemeUpdated($request));

        return redirect()->route('admin.themes.index')
            ->withFlashSuccess(__('backend_themes.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManageThemeRequest $request
     * @param Theme              $theme
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageThemeRequest $request, Theme $theme)
    {
        $this->themeRepository->deleteById($theme->id);

        // Fire delete event (ThemeDeleted)
        event(new ThemeDeleted($request));

        return redirect()->route('admin.themes.deleted')
            ->withFlashSuccess(__('backend_themes.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManageThemeRequest $request
     * @param Theme              $deletedTheme
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManageThemeRequest $request, Theme $deletedTheme)
    {
        $this->themeRepository->forceDelete($deletedTheme);

        return redirect()->route('admin.themes.deleted')
            ->withFlashSuccess(__('backend_themes.alerts.deleted_permanently'));
    }

    /**
     * @param ManageThemeRequest $request
     * @param Theme              $deletedTheme
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManageThemeRequest $request, Theme $deletedTheme)
    {
        $this->themeRepository->restore($deletedTheme);

        return redirect()->route('admin.themes.index')
            ->withFlashSuccess(__('backend_themes.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManageThemeRequest $request
     *
     * @return mixed
     */
    public function deleted(ManageThemeRequest $request)
    {
        return view('backend.theme.deleted')
            ->withthemes($this->themeRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
