<?php

namespace App\Repositories\Backend;

use App\Models\Plugin;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class PluginRepository.
 */
class PluginRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Plugin::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return Plugin
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Plugin
    {
        return DB::transaction(function () use ($data) {
            $plugin = parent::create([
                'title' => $data['title'],
            ]);

            if ($plugin) {
                return $plugin;
            }

            throw new GeneralException(__('backend_plugins.exceptions.create_error'));
        });
    }

    /**
     * @param Plugin  $plugin
     * @param array     $data
     *
     * @return Plugin
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Plugin $plugin, array $data) : Plugin
    {
        return DB::transaction(function () use ($plugin, $data) {
            if ($plugin->update([
                'title' => $data['title'],
            ])) {

                return $plugin;
            }

            throw new GeneralException(__('backend_plugins.exceptions.update_error'));
        });
    }

    /**
     * @param Plugin $plugin
     *
     * @return Plugin
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Plugin $plugin) : Plugin
    {
        if (is_null($plugin->deleted_at)) {
            throw new GeneralException(__('backend_plugins.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($plugin) {
            if ($plugin->forceDelete()) {
                return $plugin;
            }

            throw new GeneralException(__('backend_plugins.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param Plugin $plugin
     *
     * @return Plugin
     * @throws GeneralException
     */
    public function restore(Plugin $plugin) : Plugin
    {
        if (is_null($plugin->deleted_at)) {
            throw new GeneralException(__('backend_plugins.exceptions.cant_restore'));
        }

        if ($plugin->restore()) {
            return $plugin;
        }

        throw new GeneralException(__('backend_plugins.exceptions.restore_error'));
    }
}
