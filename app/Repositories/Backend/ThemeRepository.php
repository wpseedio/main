<?php

namespace App\Repositories\Backend;

use App\Models\Theme;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class ThemeRepository.
 */
class ThemeRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Theme::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return Theme
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Theme
    {
        return DB::transaction(function () use ($data) {
            $theme = parent::create([
                'title' => $data['title'],
            ]);

            if ($theme) {
                return $theme;
            }

            throw new GeneralException(__('backend_themes.exceptions.create_error'));
        });
    }

    /**
     * @param Theme  $theme
     * @param array     $data
     *
     * @return Theme
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Theme $theme, array $data) : Theme
    {
        return DB::transaction(function () use ($theme, $data) {
            if ($theme->update([
                'title' => $data['title'],
            ])) {

                return $theme;
            }

            throw new GeneralException(__('backend_themes.exceptions.update_error'));
        });
    }

    /**
     * @param Theme $theme
     *
     * @return Theme
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Theme $theme) : Theme
    {
        if (is_null($theme->deleted_at)) {
            throw new GeneralException(__('backend_themes.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($theme) {
            if ($theme->forceDelete()) {
                return $theme;
            }

            throw new GeneralException(__('backend_themes.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param Theme $theme
     *
     * @return Theme
     * @throws GeneralException
     */
    public function restore(Theme $theme) : Theme
    {
        if (is_null($theme->deleted_at)) {
            throw new GeneralException(__('backend_themes.exceptions.cant_restore'));
        }

        if ($theme->restore()) {
            return $theme;
        }

        throw new GeneralException(__('backend_themes.exceptions.restore_error'));
    }
}
