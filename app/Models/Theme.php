<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Traits\Attribute\ThemeAttribute;
use Spatie\Tags\HasTags;

class Theme extends Model
{
    use ThemeAttribute,
        HasTags,
        SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];


    /**
     * Get the versions for the theme.
     */
    public function theme_versions()
    {
        return $this->hasMany('App\Models\ThemeVersion');
    }
}
