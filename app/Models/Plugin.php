<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\Attribute\PluginAttribute;
use Spatie\Tags\HasTags;

class Plugin extends Model
{
    use PluginAttribute,
        HasTags,
        SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Get the versions for the plugin.
     */
    public function plugin_versions()
    {
        return $this->hasMany('App\Models\PluginVersion');
    }
}
