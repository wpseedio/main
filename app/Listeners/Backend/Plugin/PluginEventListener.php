<?php

namespace App\Listeners\Backend\Plugin;

/**
 * Class PluginEventListener.
 */
class PluginEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $user    = auth()->user()->name;

        $newitem = $event->plugin->title;

        \Log::info('User ' . $user . ' has created item ' . $newitem);
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        $user           = auth()->user()->name;

        $updated_item   = $event->plugin->title;

        \Log::info('User ' . $user . ' has updated item ' . $updated_item);
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        $user           = auth()->user()->name;

        $deleted_item   = $event->plugin->title;

        \Log::info('User ' . $user . ' has deleted item ' . $deleted_item);    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Plugin\PluginCreated::class,
            'App\Listeners\Backend\Plugin\PluginEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Plugin\PluginUpdated::class,
            'App\Listeners\Backend\Plugin\PluginEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Plugin\PluginDeleted::class,
            'App\Listeners\Backend\Plugin\PluginEventListener@onDeleted'
        );
    }
}
