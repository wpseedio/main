<?php

namespace App\Listeners\Backend\Theme;

/**
 * Class ThemeEventListener.
 */
class ThemeEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $user    = auth()->user()->name;

        $newitem = $event->theme->title;

        \Log::info('User ' . $user . ' has created item ' . $newitem);
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        $user           = auth()->user()->name;

        $updated_item   = $event->theme->title;

        \Log::info('User ' . $user . ' has updated item ' . $updated_item);
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        $user           = auth()->user()->name;

        $deleted_item   = $event->theme->title;

        \Log::info('User ' . $user . ' has deleted item ' . $deleted_item);    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Theme\ThemeCreated::class,
            'App\Listeners\Backend\Theme\ThemeEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Theme\ThemeUpdated::class,
            'App\Listeners\Backend\Theme\ThemeEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Theme\ThemeDeleted::class,
            'App\Listeners\Backend\Theme\ThemeEventListener@onDeleted'
        );
    }
}
