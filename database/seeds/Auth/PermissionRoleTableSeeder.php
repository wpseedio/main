<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

/**
 * Class PermissionRoleTableSeeder.
 */
class PermissionRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        $root = Role::create(['name' => config('access.users.root_role')]);
        $admin = Role::create(['name' => config('access.users.admin_role')]);
        $manager = Role::create(['name' => config('access.users.manager_role')]);
        $editor = Role::create(['name' => config('access.users.editor_role')]);
        $user = Role::create(['name' => config('access.users.default_role')]);

        // Create Permissions
        $permissions = ['view backend'];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        // Always give Root role all permissions
        $root->givePermissionTo(Permission::all());

        // Assign Permissions to other Roles
        $admin->givePermissionTo('view backend');
        $manager->givePermissionTo('view backend');
        $editor->givePermissionTo('view backend');

        $this->enableForeignKeys();
    }
}
