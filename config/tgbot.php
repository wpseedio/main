<?php

return [
    'tgbot_token' => env('TGBOT_TOKEN', false),
    'tgbot_channel' => env('TGBOT_CHANNEL', false),
];
