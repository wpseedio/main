import Model from './Model'

export default class Plugin extends Model {
    static resource()
    {
        return 'plugins'
    }
}
