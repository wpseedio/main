@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fas fa-home"></i> @lang('navs.general.plugins')
                </div>
                <div class="card-body">
                    <ul>
                        @foreach($plugins as $plugin)
                            <li class="align-middle"><a href="/admin/plugins/{{ $plugin->id }}">{{ $plugin->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
@endsection
