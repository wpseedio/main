@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fas fa-search"></i> @lang('navs.general.filter')
                </div>
                <div class="card-body">
                    <plugins-with-filter-component></plugins-with-filter-component>
                </div>
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->

    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fas fa-home"></i> @lang('navs.general.plugins')
                </div>
                <div class="card-body">
                    <plugins-all-component></plugins-all-component>
                </div>
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
@endsection
