@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_themes.labels.management'))

@section('breadcrumb-links')
    @include('backend.theme.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('backend_themes.labels.management') }} <small class="text-muted">{{ __('backend_themes.labels.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.theme.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('backend_themes.table.title')</th>
                            <th>@lang('backend_themes.table.created')</th>
                            <th>@lang('backend_themes.table.last_updated')</th>
                            <th>@lang('backend_themes.table.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($themes as $theme)
                            <tr>
                                <td class="align-middle"><a href="/admin/themes/{{ $theme->id }}">{{ $theme->title }}</a></td>
                                <td class="align-middle">{!! $theme->created_at !!}</td>
                                <td class="align-middle">{{ $theme->updated_at->diffForHumans() }}</td>
                                <td class="align-middle">{!! $theme->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $themes->count() !!} {{ trans_choice('backend_themes.table.total', $themes->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $themes->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
