<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/themes*')) }}" href="{{ route('admin.themes.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_themes.sidebar.title')
    </a>
</li>