@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_plugins.labels.management'))

@section('breadcrumb-links')
    @include('backend.plugin.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('backend_plugins.labels.management') }} <small class="text-muted">{{ __('backend_plugins.labels.deleted') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.plugin.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('backend_plugins.table.title')</th>
                            <th>@lang('backend_plugins.table.created')</th>
                            <th>@lang('backend_plugins.table.deleted')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($plugins as $plugin)
                            <tr>
                                <td class="align-middle"><a href="/admin/plugins/{{ $plugin->id }}">{{ $plugin->title }}</a></td>
                                <td class="align-middle">{!! $plugin->created_at !!}</td>
                                <td class="align-middle">{{ $plugin->deleted_at->diffForHumans() }}</td>
                                <td class="align-middle">{!! $plugin->trashed_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $plugins->count() !!} {{ trans_choice('backend_plugins.table.total', $plugins->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $plugins->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
