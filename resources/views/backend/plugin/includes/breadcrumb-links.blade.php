<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('backend_plugins.menus.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.plugins.index') }}">@lang('backend_plugins.menus.all')</a>
                <a class="dropdown-item" href="{{ route('admin.plugins.create') }}">@lang('backend_plugins.menus.create')</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.plugins.deactivated') }}">@lang('backed_plugins.menus.deactivated')</a> --}}
                <a class="dropdown-item" href="{{ route('admin.plugins.deleted') }}">@lang('backend_plugins.menus.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
