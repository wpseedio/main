<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/plugins*')) }}" href="{{ route('admin.plugins.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_plugins.sidebar.title')
    </a>
</li>