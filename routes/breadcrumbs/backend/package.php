<?php

Breadcrumbs::for('admin.packages.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_packages.labels.management'), route('admin.packages.index'));
});

Breadcrumbs::for('admin.packages.create', function ($trail) {
    $trail->parent('admin.packages.index');
    $trail->push(__('backend_packages.labels.create'), route('admin.packages.create'));
});

Breadcrumbs::for('admin.packages.show', function ($trail, $id) {
    $trail->parent('admin.packages.index');
    $trail->push(__('backend_packages.labels.view'), route('admin.packages.show', $id));
});

Breadcrumbs::for('admin.packages.edit', function ($trail, $id) {
    $trail->parent('admin.packages.index');
    $trail->push(__('backend.packages.labels.edit'), route('admin.packages.edit', $id));
});

Breadcrumbs::for('admin.packages.deleted', function ($trail) {
    $trail->parent('admin.packages.index');
    $trail->push(__('backend_packages.labels.deleted'), route('admin.packages.deleted'));
});
