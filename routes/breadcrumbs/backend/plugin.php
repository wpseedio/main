<?php

Breadcrumbs::for('admin.plugins.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_plugins.labels.management'), route('admin.plugins.index'));
});

Breadcrumbs::for('admin.plugins.create', function ($trail) {
    $trail->parent('admin.plugins.index');
    $trail->push(__('backend_plugins.labels.create'), route('admin.plugins.create'));
});

Breadcrumbs::for('admin.plugins.show', function ($trail, $id) {
    $trail->parent('admin.plugins.index');
    $trail->push(__('backend_plugins.labels.view'), route('admin.plugins.show', $id));
});

Breadcrumbs::for('admin.plugins.edit', function ($trail, $id) {
    $trail->parent('admin.plugins.index');
    $trail->push(__('backend.plugins.labels.edit'), route('admin.plugins.edit', $id));
});

Breadcrumbs::for('admin.plugins.deleted', function ($trail) {
    $trail->parent('admin.plugins.index');
    $trail->push(__('backend_plugins.labels.deleted'), route('admin.plugins.deleted'));
});
