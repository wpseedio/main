<?php

Breadcrumbs::for('admin.themes.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_themes.labels.management'), route('admin.themes.index'));
});

Breadcrumbs::for('admin.themes.create', function ($trail) {
    $trail->parent('admin.themes.index');
    $trail->push(__('backend_themes.labels.create'), route('admin.themes.create'));
});

Breadcrumbs::for('admin.themes.show', function ($trail, $id) {
    $trail->parent('admin.themes.index');
    $trail->push(__('backend_themes.labels.view'), route('admin.themes.show', $id));
});

Breadcrumbs::for('admin.themes.edit', function ($trail, $id) {
    $trail->parent('admin.themes.index');
    $trail->push(__('backend.themes.labels.edit'), route('admin.themes.edit', $id));
});

Breadcrumbs::for('admin.themes.deleted', function ($trail) {
    $trail->parent('admin.themes.index');
    $trail->push(__('backend_themes.labels.deleted'), route('admin.themes.deleted'));
});
