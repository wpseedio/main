<?php

use Dingo\Api\Routing\Router;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * @var $api Router
 */
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', ['middleware' => ['api']], function ($api) {

    /*
     * Plugins
     */
    $api->group(['prefix' => 'plugins'], function ($api) {
        $api->get('/', 'App\Api\Controllers\PluginController@index');
        $api->get('/{slug}', 'App\Api\Controllers\PluginController@show');
    });
});
