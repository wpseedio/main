<?php

use App\Http\Controllers\Backend\ThemeController;

use App\Models\Theme;

Route::bind('theme', function ($value) {
	$theme = new Theme;

	return Theme::withTrashed()->where($theme->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'themes'], function () {
	Route::get(	'', 		[ThemeController::class, 'index']		)->name('themes.index');
    Route::get(	'create', 	[ThemeController::class, 'create']	)->name('themes.create');
	Route::post('store', 	[ThemeController::class, 'store']		)->name('themes.store');
    Route::get(	'deleted', 	[ThemeController::class, 'deleted']	)->name('themes.deleted');
});

Route::group(['prefix' => 'themes/{theme}'], function () {
	// Theme
	Route::get('/', [ThemeController::class, 'show'])->name('themes.show');
	Route::get('edit', [ThemeController::class, 'edit'])->name('themes.edit');
	Route::patch('update', [ThemeController::class, 'update'])->name('themes.update');
	Route::delete('destroy', [ThemeController::class, 'destroy'])->name('themes.destroy');
	// Deleted
	Route::get('restore', [ThemeController::class, 'restore'])->name('themes.restore');
	Route::get('delete', [ThemeController::class, 'delete'])->name('themes.delete-permanently');
});