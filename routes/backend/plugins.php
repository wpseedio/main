<?php

use App\Http\Controllers\Backend\PluginController;

use App\Models\Plugin;

Route::bind('plugin', function ($value) {
    $plugin = new Plugin;

    return Plugin::withTrashed()->where($plugin->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'plugins'], function () {
    Route::get(	'', 		[PluginController::class, 'index']		)->name('plugins.index');
    Route::get(	'create', 	[PluginController::class, 'create']	)->name('plugins.create');
    Route::post('store', 	[PluginController::class, 'store']		)->name('plugins.store');
    Route::get(	'deleted', 	[PluginController::class, 'deleted']	)->name('plugins.deleted');
});

Route::group(['prefix' => 'plugins/{plugin}'], function () {
    // Plugin
    Route::get('/', [PluginController::class, 'show'])->name('plugins.show');
    Route::get('edit', [PluginController::class, 'edit'])->name('plugins.edit');
    Route::patch('update', [PluginController::class, 'update'])->name('plugins.update');
    Route::delete('destroy', [PluginController::class, 'destroy'])->name('plugins.destroy');
    // Deleted
    Route::get('restore', [PluginController::class, 'restore'])->name('plugins.restore');
    Route::get('delete', [PluginController::class, 'delete'])->name('plugins.delete-permanently');
});
